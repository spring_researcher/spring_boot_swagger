package kh.com.kshrd.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/v1/api/students")
public class StudentRestController {
	
	@RequestMapping(method=RequestMethod.GET)
	public String greeting(){
		return "HELLO SPRING BOOT SWAGGER";
	}

}
